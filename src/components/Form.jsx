import React, { useState, useEffect } from "react";
import {
  Col,
  Container,
  Row,
  Form,
  Button,
  Table,
  Image,
  Label,
} from "react-bootstrap";
import {
  fetchArticleById,
  postArticle,
  updateArticleById,
  uploadImage,
  fetchArticle,
  deleteArticle,
} from "../service/author_service";

function Post() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [id, setId] = useState("");

  const [img, setImg] = useState("");

  const [click, setClick] = useState(false);

  const [imageFile, setImageFile] = useState(null);

  const [articles, setArticles] = useState([]);

  useEffect(() => {
    const fetch = async () => {
      let articles = await fetchArticle();
      setArticles(articles);
    };
    fetch();
  }, [articles]);

  const onAdd = async (e) => {
    e.preventDefault();
    let article = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      article.image = url;
    } else {
      article.image = imageURL;
    }
    postArticle(article).then((message) => alert(message));
    setName("");
    setEmail("");
    setImageURL(
      "https://designshack.net/wp-content/uploads/placeholder-image.png"
    );
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let article = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      article.image = url;
    }
    updateArticleById(id, article).then((message) => alert(message));
    setClick(false);
    setName("");
    setEmail("");
    setImageURL(
      "https://designshack.net/wp-content/uploads/placeholder-image.png"
    );
  };

  const onDelete = async (id) => {
    deleteArticle(id)
      .then((message) => {
        let newArticles = articles.filter((article) => article._id !== id);
        setArticles(newArticles);
        alert(message);
      })
      .catch((e) => console.log(e));
  };

  const onEdit = (id) => {
    setClick(true);
    setId(id);
    let tmp = articles.filter((item) => {
      return item._id === id;
    });
    setName(tmp[0].name);
    setEmail(tmp[0].email);
    setImg(tmp[0].image);
  };

  return (
    <Container>
      <Col>
        <h1>Author</h1>
      </Col>
      <br />
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="title">
              <Form.Label>Author Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>
            <br />
            <Form.Group controlId="title">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>
            <br />
            <Button
              variant="primary"
              type="submit"
              onClick={click ? onUpdate : onAdd}
            >
              {click ? "Save" : "Submit"}
            </Button>
          </Form>
        </Col>
        <Col md={4}>
          <img className="w-100" src={click ? img : imageURL} />
          <Form>
            <Form.Group>
              <Form.Label>Choose Image</Form.Label>
              <Form.File
                id="img"
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageURL(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>

      <Row>
        <div style={{ marginTop: "25px" }}>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {articles.map((item, i) => (
                <tr>
                  <td>{i + 1}</td>
                  <td>{item.name}</td>
                  <td>{item.email}</td>
                  <td>
                    <Image
                      src={item.image}
                      rounded
                      style={{
                        objectFit: "scale-down",
                        width: "100px",
                        height: "50px",
                      }}
                    />
                  </td>
                  <td>
                    <Button
                      size="sm"
                      variant="warning"
                      onClick={() => {
                        onEdit(item._id);
                      }}
                    >
                      Edit
                    </Button>{" "}
                    <Button
                      disabled={click ? "disable" : ""}
                      size="sm"
                      variant="danger"
                      onClick={() => onDelete(item._id)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}{" "}
            </tbody>
          </Table>
        </div>
      </Row>
    </Container>
  );
}

export default Post;

import api from "../api/api"

export const fetchArticle = async () => {
    let response = await api.get('author')
    return response.data.data
}

export const deleteArticle = async (id) => {
    let response = await api.delete('author/' + id)
    return response.data.message
}

export const postArticle = async (article) => {
    let response = await api.post('author', article)
    return response.data.message
}

export const fetchArticleById = async (id) => {
    let response = await api.get('author/' + id)
    return response.data.data
}

export const updateArticleById = async (id, newArticle) => {
    let response = await api.put('author/' + id, newArticle)
    return response.data.message
}

export const uploadImage = async (file) => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await api.post('images', formData)
    return response.data.url
}